```
USAGE
       locale-gen

PATHS
          Supported
              /usr/share/i18n/SUPPORTED

          To generate
              /etc/locale.gen

          Generated
              /usr/lib/locale
